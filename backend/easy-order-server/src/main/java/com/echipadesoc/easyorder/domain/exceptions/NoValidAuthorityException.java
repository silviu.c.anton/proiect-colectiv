package com.echipadesoc.easyorder.domain.exceptions;

public class NoValidAuthorityException extends RuntimeException {
    public NoValidAuthorityException(String message) {
        super(message);
    }
}

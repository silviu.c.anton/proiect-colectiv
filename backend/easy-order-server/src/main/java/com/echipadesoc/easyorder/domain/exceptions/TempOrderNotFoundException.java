package com.echipadesoc.easyorder.domain.exceptions;

public class TempOrderNotFoundException extends RuntimeException {

    public TempOrderNotFoundException(String message) {
        super(message);
    }
}

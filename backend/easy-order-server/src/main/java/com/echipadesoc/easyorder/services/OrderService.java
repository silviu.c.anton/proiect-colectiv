package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.Waiter;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayOrderDTO;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;

import java.util.List;

public interface OrderService {
    List<DisplayOrderDTO> getOrdersByWaiterIdWithStatus(long WaiterId, boolean isDone);

    DisplayOrderDTO saveOrder(OrderDTO orderDTO, Waiter waiter);

    void updateOrderStatus(Long id, boolean newStatus);
}

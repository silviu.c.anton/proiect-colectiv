package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.dtos.MenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuSectionDTO;

import java.util.List;

public interface MenuService {
    List<MenuSectionDTO> getMenu();

    MenuItemDTO updateMenuItem(MenuItemDTO menuItem);

    MenuItemDTO addMenuItem(MenuItemDTO menuItemDTO);

    List<MenuItemDTO> getMenuItems(Integer page, Integer size, String sortBy);

    MenuItemDTO getMenuItemById(Long id);

    void deleteMenuItem(Long id);
}

package com.echipadesoc.easyorder.repositories;

import com.echipadesoc.easyorder.domain.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    public List<Authority> findByNameIn(List<String> names);
}

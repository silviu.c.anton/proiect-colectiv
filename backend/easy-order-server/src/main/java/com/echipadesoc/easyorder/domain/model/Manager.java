package com.echipadesoc.easyorder.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;

@EqualsAndHashCode(callSuper = true)
@Data
@javax.persistence.Entity
@DiscriminatorValue("Manager")
public class Manager extends Employee {
}

package com.echipadesoc.easyorder.domain.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebSocketNotification<T> {
    private String event;
    private Payload<T> payload;
}
package com.echipadesoc.easyorder.domain.utils;

import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.Order;
import com.echipadesoc.easyorder.domain.model.TempOrder;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayMenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayOrderDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;
import com.echipadesoc.easyorder.domain.model.dtos.TempOrderDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DtoMapper {
    public TempOrderDTO tempOrderToTempOrderDto(TempOrder tempOrder) {
        return new TempOrderDTO(
                tempOrder.getId(),
                tempOrder.getNickname(),
                tempOrder.getMenuItems().stream().map(MenuItem::getId).collect(Collectors.toList()),
                tempOrder.getSessionId()
        );
    }

    public MenuItemDTO menuItemToMenuItemDto(MenuItem menuItem) {
        byte[] photo = FileUtils.readImageToByteArray(menuItem.getPhotoPath());
        return new MenuItemDTO(
                menuItem.getId(),
                menuItem.getName(),
                menuItem.getDescription(),
                menuItem.getPrice(),
                menuItem.getPhotoPath(),
                photo,
                menuItem.isAvailable(),
                menuItem.getCategory().getName()
        );
    }

    /* If needed someday
    public MenuItem displayMenuItemDtoToMenuItem(DisplayMenuItemDTO displayMenuItemDTO) {
        return new MenuItem();
    }
    */

    public OrderDTO orderToOrderDto(Order order) {
        List<Long> menuItemIds = order.getMenuItems()
                .stream()
                .map(MenuItem::getId)
                .collect(Collectors.toList());
        return new OrderDTO(
                order.getId(),
                order.getComments(),
                order.getTable().getId(),
                menuItemIds
        );
    }

    /* If needed someday
    public MenuItem menuItemDtoToMenuItem(MenuItemDTO menuItemDTO) {
        return new MenuItem();
    }
    */

    public DisplayOrderDTO orderToDisplayOrderDTO(Order order) {
        return new DisplayOrderDTO(
                order.getId(),
                order.getComments(),
                order.getTable().getId(),
                order.getOrderDate().toString(),
                order.getMenuItems().stream()
                        .map(this::menuItemToDisplayMenuItemDto)
                        .collect(Collectors.toList())
        );
    }

    /* If needed someday
    public Order OrderDtoToOrder(OrderDTO orderDTO) {
        return new Order();
    }
    */

    public DisplayMenuItemDTO menuItemToDisplayMenuItemDto(MenuItem menuItem) {
        return new DisplayMenuItemDTO(
                menuItem.getId(),
                menuItem.getName(),
                menuItem.getDescription(),
                menuItem.getPrice(),
                menuItem.getPhotoPath(),
                menuItem.isAvailable()
        );
    }
}

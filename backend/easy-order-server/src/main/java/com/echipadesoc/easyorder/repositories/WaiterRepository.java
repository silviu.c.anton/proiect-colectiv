package com.echipadesoc.easyorder.repositories;

import com.echipadesoc.easyorder.domain.model.Waiter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WaiterRepository extends JpaRepository<Waiter, Long> {
    Optional<Waiter> findByAccount_Username(String username);
}

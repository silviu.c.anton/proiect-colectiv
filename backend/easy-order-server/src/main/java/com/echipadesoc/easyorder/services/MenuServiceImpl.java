package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.MenuItemNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.MenuItemUsedByOrderException;
import com.echipadesoc.easyorder.domain.model.Category;
import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayMenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuSectionDTO;
import com.echipadesoc.easyorder.domain.utils.DtoMapper;
import com.echipadesoc.easyorder.repositories.CategoryRepository;
import com.echipadesoc.easyorder.repositories.MenuItemRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MenuServiceImpl implements MenuService {
    private final MenuItemRepository menuItemRepository;
    private final CategoryRepository categoryRepository;
    private final DtoMapper dtoMapper;

    public MenuServiceImpl(MenuItemRepository menuItemRepository, CategoryRepository categoryRepository, DtoMapper dtoMapper) {
        this.menuItemRepository = menuItemRepository;
        this.categoryRepository = categoryRepository;
        this.dtoMapper = dtoMapper;
    }

    /**
     * Returns a list of Menu Section DTO grouped by category
     */
    @Override
    public List<MenuSectionDTO> getMenu() {
        List<MenuItem> menuItems = menuItemRepository.getAllByCategoryNotNull();

        Map<String, List<MenuItem>> itemsGroupedByCategory = menuItems.stream()
                .collect(Collectors.groupingBy(item -> item.getCategory().getName()));

        return createMenu(itemsGroupedByCategory);
    }

    /**
     * Map Database Entity to DTO.
     */
    public List<MenuSectionDTO> createMenu(Map<String, List<MenuItem>> itemsGroupedByCategory) {
        List<MenuSectionDTO> menu = new ArrayList<>();

        for (Map.Entry<String, List<MenuItem>> keyValuePair : itemsGroupedByCategory.entrySet()) {
            List<DisplayMenuItemDTO> menuItemDTOs = keyValuePair.getValue()
                    .stream()
                    .map(dtoMapper::menuItemToDisplayMenuItemDto)
                    .collect(Collectors.toList());

            MenuSectionDTO menuSectionDTO = new MenuSectionDTO(keyValuePair.getKey(), menuItemDTOs);
            menu.add(menuSectionDTO);
        }

        return menu;
    }

    /**
     * Update a Menu Item. Check if it exists in the Database.
     * The category gets created if it is new.
     */
    public MenuItemDTO updateMenuItem(MenuItemDTO menuItem) {
        if (menuItemRepository.findById(menuItem.getId()).isEmpty()) {
            throw new MenuItemNotFoundException("The menu item with the given ID was not found!");
        }

        Optional<Category> categoryOpt = categoryRepository.findByName(menuItem.getCategory());
        final MenuItem newItem = new MenuItem(menuItem.getId(),
                                              menuItem.getName(),
                                              menuItem.getDescription(),
                                              menuItem.getPrice(),
                                              menuItem.getPhotoPath(),
                                              menuItem.isAvailable(),
                                              null);

        categoryOpt.ifPresentOrElse(newItem::setCategory,
                                    () -> newItem.setCategory(categoryRepository.save(new Category(0L, menuItem.getCategory()))));

        return dtoMapper.menuItemToMenuItemDto(menuItemRepository.save(newItem));
    }

    /**
     * Save a Menu Item.
     * The category gets created if it is new.
     */
    @Override
    public MenuItemDTO addMenuItem(MenuItemDTO menuItemDTO) {
        Optional<Category> categoryOpt = categoryRepository.findByName(menuItemDTO.getCategory());
        final MenuItem newItem = new MenuItem(
                0L,
                menuItemDTO.getName(),
                menuItemDTO.getDescription(),
                menuItemDTO.getPrice(),
                menuItemDTO.getPhotoPath(),
                menuItemDTO.isAvailable(),
                null);
        categoryOpt.ifPresentOrElse(newItem::setCategory,
                                    () -> newItem.setCategory(categoryRepository.save(new Category(0L, menuItemDTO.getCategory()))));

        return dtoMapper.menuItemToMenuItemDto(menuItemRepository.save(newItem));
    }

    /**
     * Get menu items paged.
     * If size is not specified, all table entities are returned.
     * The items are sorted if specified.
     */
    @Override
    public List<MenuItemDTO> getMenuItems(Integer page, Integer size, String sortBy) {
        Optional<Integer> pageOptional = Optional.ofNullable(page);
        Optional<Integer> sizeOptional = Optional.ofNullable(size);
        Optional<String> sortByOptional = Optional.ofNullable(sortBy);

        if (pageOptional.isEmpty() && sizeOptional.isPresent()) {
            throw new IllegalArgumentException("While page is null size cannot have a value");
        }

        if (pageOptional.isPresent() && sizeOptional.isEmpty()) {
            sizeOptional = Optional.of(5);
        }

        Pageable pageable = PageRequest.of(
                pageOptional.orElse(0),
                sizeOptional.orElse(Integer.MAX_VALUE),
                sortByOptional.map(sort -> Sort.by(getSorting(sort))).orElse(Sort.unsorted())
        );
        return menuItemRepository.findAll(pageable)
                .getContent()
                .stream()
                .map(dtoMapper::menuItemToMenuItemDto)
                .collect(Collectors.toList());
    }

    /**
     * Maps a string to possible sorting patterns.
     * E.g.: name.asc creates a Sort By Name Ascending Query.
     */
    public List<Sort.Order> getSorting(String sortBy) {
        return Arrays.stream(sortBy.split(","))
                .map(item -> item.split("\\."))
                .map(item -> new Sort.Order(Sort.Direction.fromString(item[1]), item[0]))
                .collect(Collectors.toList());

    }

    /**
     * Return an item by id or throw error if it does not exist.
     */
    @Override
    public MenuItemDTO getMenuItemById(Long id) {
        Optional<MenuItem> menuItemOpt = menuItemRepository.findById(id);
        return menuItemOpt
                .map(dtoMapper::menuItemToMenuItemDto)
                .orElseThrow(() -> new MenuItemNotFoundException("The menu item with the given ID was not found!"));
    }

    /**
     * Delete an item or throw an error if it is does not exist or if it is linked to an order.
     */
    @Override
    public void deleteMenuItem(Long id) {
        if (!menuItemRepository.existsById(id))
            throw new MenuItemNotFoundException("The menu item with the given ID was not found!");
        try {
            menuItemRepository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new MenuItemUsedByOrderException("The menu item with the given ID was ordered and can't be deleted!");
        }
    }
}

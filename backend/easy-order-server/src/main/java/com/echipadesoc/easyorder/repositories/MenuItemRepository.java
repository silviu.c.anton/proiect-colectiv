package com.echipadesoc.easyorder.repositories;

import com.echipadesoc.easyorder.domain.model.MenuItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {
    List<MenuItem> getAllByCategoryNotNull();
}

package com.echipadesoc.easyorder.webservices.exceptionHandlers;

import com.echipadesoc.easyorder.domain.exceptions.InsufficientFundsException;
import com.echipadesoc.easyorder.domain.exceptions.InvalidCreditCardException;
import com.echipadesoc.easyorder.domain.exceptions.MenuItemNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.MenuItemUsedByOrderException;
import com.echipadesoc.easyorder.domain.exceptions.NoValidAuthorityException;
import com.echipadesoc.easyorder.domain.exceptions.OrderNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.SameOrderStatusException;
import com.echipadesoc.easyorder.domain.exceptions.TableNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.TempOrderConflictException;
import com.echipadesoc.easyorder.domain.exceptions.TempOrderNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.UserAlreadyExistsException;
import com.echipadesoc.easyorder.domain.exceptions.WaiterNotFoundException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionToResponseMapper extends ResponseEntityExceptionHandler {

    private ResponseEntity<ErrorResponse> getErrorResponse(RuntimeException exception, HttpStatus status) {
        ErrorResponse errorResponse = new ErrorResponse(status.value(),
                                                        exception.getMessage(),
                                                        System.currentTimeMillis());

        return new ResponseEntity<>(errorResponse, status);
    }

    @ExceptionHandler(value = TempOrderConflictException.class)
    public ResponseEntity<ErrorResponse> handleTempOrderConflict(TempOrderConflictException tempOrderConflictException) {
        return getErrorResponse(tempOrderConflictException, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = TableNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleTableNotFound(TableNotFoundException tableNotFoundException) {
        return getErrorResponse(tableNotFoundException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> handleBadCredentials(BadCredentialsException badCredentialsException) {
        return getErrorResponse(badCredentialsException, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleUsernameNotFound(UsernameNotFoundException usernameNotFoundException) {
        return getErrorResponse(usernameNotFoundException, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = MalformedJwtException.class)
    public ResponseEntity<ErrorResponse> handleMalformedJwt(MalformedJwtException malformedJwtException) {
        return getErrorResponse(malformedJwtException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public ResponseEntity<ErrorResponse> handleExpiredJwt(ExpiredJwtException expiredJwtException) {
        return getErrorResponse(expiredJwtException, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = MenuItemNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleMenuItemNotFound(MenuItemNotFoundException menuItemNotFoundException) {
        return getErrorResponse(menuItemNotFoundException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArguments(IllegalArgumentException illegalArgumentException) {
        return getErrorResponse(illegalArgumentException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoValidAuthorityException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArguments(NoValidAuthorityException noValidAuthorityException) {
        return getErrorResponse(noValidAuthorityException, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArguments(UserAlreadyExistsException userAlreadyExistsException) {
        return getErrorResponse(userAlreadyExistsException, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = MenuItemUsedByOrderException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArguments(MenuItemUsedByOrderException menuItemUsedByOrderException) {
        return getErrorResponse(menuItemUsedByOrderException, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = WaiterNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleWaiterNotFound(WaiterNotFoundException waiterNotFoundException) {
        return getErrorResponse(waiterNotFoundException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = SameOrderStatusException.class)
    public ResponseEntity<ErrorResponse> handleSameOrderStatus(SameOrderStatusException sameOrderStatusException) {
        return getErrorResponse(sameOrderStatusException, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = OrderNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleOrderNotFound(OrderNotFoundException orderNotFoundException) {
        return getErrorResponse(orderNotFoundException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TempOrderNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleTempOrderNotFound(TempOrderNotFoundException tempOrderNotFoundException) {
        return getErrorResponse(tempOrderNotFoundException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InsufficientFundsException.class)
    public ResponseEntity<ErrorResponse> handleTempOrderNotFound(InsufficientFundsException insufficientFundsException) {
        return getErrorResponse(insufficientFundsException, HttpStatus.PAYMENT_REQUIRED);
    }

    @ExceptionHandler(value = InvalidCreditCardException.class)
    public ResponseEntity<ErrorResponse> handleTempOrderNotFound(InvalidCreditCardException invalidCreditCardException) {
        return getErrorResponse(invalidCreditCardException, HttpStatus.NOT_ACCEPTABLE);
    }
}

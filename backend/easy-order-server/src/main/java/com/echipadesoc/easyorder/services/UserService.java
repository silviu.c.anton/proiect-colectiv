package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.User;
import com.echipadesoc.easyorder.repositories.UserRepository;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User saveUser(User user);

    List<String> getAuthoritiesByUsername(String username);

    Optional<Long> getEmployeeIdByUsername(String username);
}

package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.MenuItemNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.OrderNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.SameOrderStatusException;
import com.echipadesoc.easyorder.domain.exceptions.TableNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.WaiterNotFoundException;
import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.Order;
import com.echipadesoc.easyorder.domain.model.Table;
import com.echipadesoc.easyorder.domain.model.Waiter;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayOrderDTO;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;
import com.echipadesoc.easyorder.domain.utils.DtoMapper;
import com.echipadesoc.easyorder.repositories.MenuItemRepository;
import com.echipadesoc.easyorder.repositories.OrderRepository;
import com.echipadesoc.easyorder.repositories.TableRepository;
import com.echipadesoc.easyorder.repositories.WaiterRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final MenuItemRepository menuItemRepository;
    private final TableRepository tableRepository;
    private final WaiterRepository waiterRepository;
    private final DtoMapper dtoMapper;

    public OrderServiceImpl(OrderRepository orderRepository, MenuItemRepository menuItemRepository,
                            TableRepository tableRepository, WaiterRepository waiterRepository,
                            DtoMapper dtoMapper) {
        this.orderRepository = orderRepository;
        this.menuItemRepository = menuItemRepository;
        this.tableRepository = tableRepository;
        this.waiterRepository = waiterRepository;
        this.dtoMapper = dtoMapper;
    }

    /**
     * Get all orders handled by a waiter with a certain status or throw an error if the waiter does not exist.
     */
    @Override
    public List<DisplayOrderDTO> getOrdersByWaiterIdWithStatus(long waiterId, boolean isDone) {
        if (waiterRepository.existsById(waiterId)) {
            return orderRepository.findAllByWaiter_IdAndDone(waiterId, isDone).stream()
                    .map(dtoMapper::orderToDisplayOrderDTO)
                    .collect(Collectors.toList());
        }
        throw new WaiterNotFoundException("The waiter with the given id doesn't exist.");
    }

    /**
     * Assign a order to a waiter.
     * Check if the table id is valid.
     * Check if all items for the order exists in the database.
     */
    @Override
    public DisplayOrderDTO saveOrder(OrderDTO orderDTO, Waiter waiter) {
        Optional<Table> foundTable = tableRepository.findById(orderDTO.getTableId());
        if (foundTable.isEmpty()) {
            throw new TableNotFoundException("The table could not be found!");
        }

        List<MenuItem> menuItemList = orderDTO.getMenuItemIds()
                .stream()
                .map(menuItemRepository::findById)
                .map(optMenuItem -> optMenuItem.orElseThrow(() -> new MenuItemNotFoundException("The menu item could not be found!")))
                .collect(Collectors.toList());

        Order order = new Order(
                0,
                orderDTO.getComments(),
                LocalDateTime.now(),
                false,
                foundTable.get(),
                waiter,
                menuItemList
        );

        order.setOrderDate(LocalDateTime.now());
        return dtoMapper.orderToDisplayOrderDTO(orderRepository.save(order));
    }

    /**
     * Change an order status or throw an error if the status is already set or if the order does not exist.
     */
    public void updateOrderStatus(Long id, boolean newStatus) {
        orderRepository.findById(id).ifPresentOrElse(foundOrder -> {
            if (foundOrder.isDone() == newStatus) {
                throw new SameOrderStatusException("The order already has this status");
            }
            foundOrder.setDone(newStatus);
            orderRepository.save(foundOrder);
        }, () -> {
            throw new OrderNotFoundException("The order with the given ID was not found!");
        });
    }
}

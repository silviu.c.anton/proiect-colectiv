package com.echipadesoc.easyorder.webservices;

import com.echipadesoc.easyorder.domain.exceptions.WaiterNotFoundException;
import com.echipadesoc.easyorder.domain.model.Waiter;
import com.echipadesoc.easyorder.services.WaiterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("waiters")
@Api(value = "/waiters", produces = MediaType.APPLICATION_JSON_VALUE)
public class WaiterController {

    private final WaiterService waiterService;

    public WaiterController(WaiterService waiterService) {
        this.waiterService = waiterService;
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden - Needs WAITER authority"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "System Error")
    })
    @ApiOperation(value = "Get the id of the waiter with the given username",
            response = Long.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/idByUsername")
    @PreAuthorize("hasAuthority('WAITER')")
    public Long getWaiterIdByUsername(
            @ApiParam(name = "username", type = "String",
                    value = "The username of the waiter whose id is to be obtained",
                    example = "waiter")
            @RequestParam(value = "username") String username) {
        Optional<Waiter> waiterOpt = waiterService.getWaiterByUsername(username);
        return waiterOpt.map(Waiter::getId)
                .orElseThrow(() -> new WaiterNotFoundException("Waiter with given username was not found"));
    }
}

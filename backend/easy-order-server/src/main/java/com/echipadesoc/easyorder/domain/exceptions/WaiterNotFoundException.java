package com.echipadesoc.easyorder.domain.exceptions;

public class WaiterNotFoundException extends RuntimeException {
    public WaiterNotFoundException(String message) {
        super(message);
    }
}

package com.echipadesoc.easyorder.webservices;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Api(value = "/data", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
@Slf4j
@RequestMapping("/data")
@RestController
public class FileStorageController {

    @GetMapping("/{path:.+}")
    public byte[] downloadFile(@PathVariable String path) throws IOException {
        Resource resource = new ClassPathResource("/data/" + path);
        return IOUtils.toByteArray(resource.getInputStream());
    }
}

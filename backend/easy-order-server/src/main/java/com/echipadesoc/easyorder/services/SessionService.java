package com.echipadesoc.easyorder.services;

public interface SessionService {
    String generateNewSessionId();
}

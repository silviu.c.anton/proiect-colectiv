package com.echipadesoc.easyorder.domain.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SecurityUser extends User {
    public SecurityUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public com.echipadesoc.easyorder.domain.model.User toAppUser() {
        List<Authority> authorities = new ArrayList<>();
        this.getAuthorities().forEach(grantedAuthority -> authorities.add(new Authority(0L, grantedAuthority.getAuthority())));

        return new com.echipadesoc.easyorder.domain.model.User(0, getUsername(), getPassword(), null, authorities);
    }
}

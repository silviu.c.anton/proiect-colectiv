package com.echipadesoc.easyorder.domain.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MasterClientConnectMessage {
    private String sessionId;
}

package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.TempOrderConflictException;
import com.echipadesoc.easyorder.domain.exceptions.TempOrderNotFoundException;
import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.TempOrder;
import com.echipadesoc.easyorder.domain.model.dtos.TempOrderDTO;
import com.echipadesoc.easyorder.domain.utils.DtoMapper;
import com.echipadesoc.easyorder.repositories.MenuItemRepository;
import com.echipadesoc.easyorder.repositories.TempOrderRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TempOrderServiceImpl implements TempOrderService {

    private final TempOrderRepository tempOrderRepository;
    private final MenuItemRepository menuItemRepository;
    private final DtoMapper dtoMapper;

    public TempOrderServiceImpl(TempOrderRepository tempOrderRepository, DtoMapper dtoMapper,
                                MenuItemRepository menuItemRepository) {
        this.tempOrderRepository = tempOrderRepository;
        this.menuItemRepository = menuItemRepository;
        this.dtoMapper = new DtoMapper();
    }

    /**
     * Save a new temporary order or throw if the guest name of the client already exists in the session.
     */
    @Override
    public TempOrderDTO storeTempOrder(TempOrderDTO tempOrderDTO) {
        if (tempOrderRepository.existsByNicknameAndSessionId(tempOrderDTO.getNickname(), tempOrderDTO.getSessionId())) {
            throw new TempOrderConflictException("The name is already present in other tempOrder!");
        }

        TempOrder tempOrder = new TempOrder();
        tempOrder.setId(0);
        tempOrder.setSessionId(tempOrderDTO.getSessionId());
        tempOrder.setNickname(tempOrderDTO.getNickname());
        tempOrder.setMenuItems(getMenuItemsByIds(tempOrderDTO.getMenuItemIds()));

        return dtoMapper.tempOrderToTempOrderDto(tempOrderRepository.save(tempOrder));
    }

    private List<MenuItem> getMenuItemsByIds(List<Long> menuItemIds) {
        List<MenuItem> menuItems = new ArrayList<>();
        for (Long id : menuItemIds) {
            menuItems.add(this.menuItemRepository.getOne(id));
        }
        return menuItems;
    }

    @Override
    public void deleteTempOrderBySessionId(String sessionId) {
        List<TempOrder> tempOrders = tempOrderRepository.getAllBySessionId(sessionId);
        for (TempOrder tempOrder : tempOrders) {
            tempOrderRepository.delete(tempOrder);
        }
    }

    @Override
    public List<TempOrder> getAllBySessionId(String sessionId) {
        return tempOrderRepository.getAllBySessionId(sessionId);
    }

    /**
     * Update a temporary order or throw an error if it does not exist.
     */
    @Override
    public TempOrderDTO updateTempOrder(TempOrderDTO tempOrderDTO) {
        TempOrder tempOrder = new TempOrder();
        tempOrder.setSessionId(tempOrderDTO.getSessionId());
        tempOrder.setNickname(tempOrderDTO.getNickname());
        tempOrder.setMenuItems(getMenuItemsByIds(tempOrderDTO.getMenuItemIds()));

        tempOrder.setId(tempOrderRepository
                .findBySessionIdAndNickname(tempOrderDTO.getSessionId(), tempOrder.getNickname()).map(TempOrder::getId)
                .orElseThrow(() -> new TempOrderNotFoundException(
                        String.format(
                                "Temporary Order for the session %s with the nickname %s does not exist!",
                                tempOrderDTO.getSessionId(),
                                tempOrderDTO.getNickname()
                        ))));

        return dtoMapper.tempOrderToTempOrderDto(tempOrderRepository.save(tempOrder));
    }
}

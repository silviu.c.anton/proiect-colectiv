package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.Waiter;

import java.util.Optional;

public interface WaiterService {
    Optional<Waiter> getWaiterById(Long id);

    Optional<Waiter> getWaiterByUsername(String username);
}

package com.echipadesoc.easyorder.repositories;

import com.echipadesoc.easyorder.domain.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByWaiter_IdAndDone(long id,boolean done);
}

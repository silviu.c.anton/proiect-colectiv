package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.Waiter;
import com.echipadesoc.easyorder.repositories.WaiterRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WaiterServiceImpl implements WaiterService {
    private final WaiterRepository waiterRepository;

    public WaiterServiceImpl(WaiterRepository waiterRepository) {
        this.waiterRepository = waiterRepository;
    }

    @Override
    public Optional<Waiter> getWaiterById(Long id) {
        return waiterRepository.findById(id);
    }

    @Override public Optional<Waiter> getWaiterByUsername(String username) {
        return waiterRepository.findByAccount_Username(username);
    }
}

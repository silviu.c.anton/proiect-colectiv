package com.echipadesoc.easyorder.domain.exceptions;

public class TempOrderConflictException extends RuntimeException {
    public TempOrderConflictException(String message) {
        super(message);
    }
}

package com.echipadesoc.easyorder.domain.model.dtos;

public class WebSocketEvents {

    public static String NEW_ORDER_EVENT = "NEW_ORDER";
    public static String NEW_CLIENT_EVENT = "NEW_CLIENT_EVENT";
    public static String NEW_TEMP_ORDER_EVENT = "NEW_TEMP_ORDER_EVENT";
}

package com.echipadesoc.easyorder.domain.exceptions;

public class MenuItemUsedByOrderException extends RuntimeException {
    public MenuItemUsedByOrderException(String message) {
        super(message);
    }
}

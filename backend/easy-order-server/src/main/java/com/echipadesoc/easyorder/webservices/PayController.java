package com.echipadesoc.easyorder.webservices;

import com.echipadesoc.easyorder.domain.exceptions.InsufficientFundsException;
import com.echipadesoc.easyorder.domain.exceptions.InvalidCreditCardException;
import com.echipadesoc.easyorder.domain.model.dtos.CreditCardDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(value = "/pay", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequestMapping("/pay")
@RestController
public class PayController {

    /**
     * !!!IT'S A MOCK!!!
     * It checks if the given credit card information is correct and pays for the given order.
     */
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 402, message = "Insufficient funds"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Invalid credit card"),
            @ApiResponse(code = 500, message = "System Error")
    })
    @ApiOperation(value = "pays an order")
    @PostMapping
    public void postPay(@Valid @RequestBody CreditCardDTO creditCardDTO) {
        log.debug("Entered class = PayController & method = postPay");
        if (creditCardDTO.getCardNumber() == null || creditCardDTO.getCardNumber().isEmpty()
                || creditCardDTO.getOwner() == null || creditCardDTO.getOwner().isEmpty()
                || creditCardDTO.getExpirationDate() == null || creditCardDTO.getExpirationDate().isEmpty())
            throw new InvalidCreditCardException("Invalid credit card");
        if (creditCardDTO.getCvv() < 500)
            throw new InsufficientFundsException("Insufficient funds");
    }
}

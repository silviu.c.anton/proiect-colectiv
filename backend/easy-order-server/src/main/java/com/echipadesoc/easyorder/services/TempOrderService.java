package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.model.TempOrder;
import com.echipadesoc.easyorder.domain.model.dtos.TempOrderDTO;

import java.util.List;

public interface TempOrderService {
    TempOrderDTO storeTempOrder(TempOrderDTO tempOrderDTO);

    void deleteTempOrderBySessionId(String sessionId);

    List<TempOrder> getAllBySessionId(String sessionId);

    TempOrderDTO updateTempOrder(TempOrderDTO tempOrderDTO);
}

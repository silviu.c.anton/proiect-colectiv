package com.echipadesoc.easyorder.repositories;

import com.echipadesoc.easyorder.domain.model.Table;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TableRepository extends JpaRepository<Table, Long> {
}

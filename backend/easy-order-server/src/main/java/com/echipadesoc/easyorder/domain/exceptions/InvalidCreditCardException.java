package com.echipadesoc.easyorder.domain.exceptions;

public class InvalidCreditCardException extends RuntimeException {
    public InvalidCreditCardException(String message) {
        super(message);
    }
}
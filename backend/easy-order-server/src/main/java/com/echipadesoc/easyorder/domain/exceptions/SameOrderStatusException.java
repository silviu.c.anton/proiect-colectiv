package com.echipadesoc.easyorder.domain.exceptions;

public class SameOrderStatusException extends RuntimeException{
    public SameOrderStatusException(String message) {
        super(message);
    }
}

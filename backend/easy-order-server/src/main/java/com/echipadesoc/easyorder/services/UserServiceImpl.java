package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.NoValidAuthorityException;
import com.echipadesoc.easyorder.domain.exceptions.UserAlreadyExistsException;
import com.echipadesoc.easyorder.domain.model.Authority;
import com.echipadesoc.easyorder.domain.model.User;
import com.echipadesoc.easyorder.repositories.AuthorityRepository;
import com.echipadesoc.easyorder.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    public UserServiceImpl(UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    /**
     * Saves a new account into the database
     */
    @Override
    public User saveUser(User user) {
        user.setId(0);
        List<Authority> authorities = authorityRepository.findByNameIn(user.getAuthorities()
                .stream()
                .map(Authority::getName)
                .collect(Collectors.toList()));

        if (authorities.isEmpty()) {
            throw new NoValidAuthorityException("None of the given authorities is valid");
        }

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new UserAlreadyExistsException("A user with the given username already exists.");
        }

        user.setAuthorities(authorities);
        return userRepository.save(user);
    }

    @Override
    public List<String> getAuthoritiesByUsername(String username) {
        return userRepository.findByUsername(username).map(User::getAuthoritiesAsStrings)
                .orElse(Collections.emptyList());
    }

    @Override public Optional<Long> getEmployeeIdByUsername(String username) {
        return userRepository.findByUsername(username).map(user -> user.getEmployee().getId()).or(Optional::empty);
    }
}

package com.echipadesoc.easyorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyOrderServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(EasyOrderServerApplication.class, args);
    }
}

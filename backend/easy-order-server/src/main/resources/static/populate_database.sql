/*
TODO: WARNING! THE FOLLOWING SCRIPT WHEN RUN WILL ERASE ALL YOUR DATA FROM THE LOCAL DATABASE
    YOU HAVE BEEN WARNED! PLEASE MAKE SURE YOU DO NOT LOSE ANY IMPORTANT DATA!
    ALSO, ALL THE IDs USED IN THE INTEGRATION TESTS AND THE FOLLOWING SCRIPT MUST BE NEGATIVE NUMBERS!
*/
/*=====================================================================================================================
  NAME                  TICKET          DATE            DESCRIPTION
  Ciubotariu George     APP-32          08.11.2020      Added table with no assigned waiter to use in tests
  Ciubotariu George     APP-46          12.11.2020      Removed waiterId from Table
  Ciubotariu George     APP-61          10.12.2020      Added truncate for new "temp" tables
===================================================================================================================== */

truncate table orders_menu_items CASCADE;
truncate table orders CASCADE;
truncate table employees CASCADE;
truncate table users CASCADE;
truncate table employees CASCADE;
truncate table authorities CASCADE;
truncate table tables CASCADE;
truncate table categories CASCADE;
truncate table users_authorities CASCADE;
truncate table menu_items CASCADE;
truncate table temp_orders CASCADE;
truncate table temp_orders_menu_items CASCADE;
-- there is no need for every cascade

insert into categories(id, name)
values (-1, 'drinks'),
       (-2, 'breakfast'),
       (-3, 'salad'),
       (-4, 'pizza'),
       (-5, 'desert');

insert into authorities(id, authority) values
(-1,'MANAGER'),
(-2,'WAITER');

insert into users(id, username, password, employee_id) values
(-1,'manager', '$2a$10$k0x3EbmiEhONRppynCuGOuLqqJD2nplGNw.Q4RJ6fvXnBpeYW6lFa', null), --manager
(-2,'waiter', '$2a$10$aFWtjdz8Ptr6uSYH.gc54uNAgmucd8uvBq6RKer6./VAfSZkwfwbu', null), --waiter
(-3,'ioana', '$2a$10$oCD0PIzQ6czi48NqTF.v9uB5h11FqDNBD8l7VfJET8yA4JVjHsceG', null), --iopass
(-4,'john', '$2a$10$CU1tFE3/MAVwAhH2z0eL2exIatTATemJ1L32fKms4UNHr.i.BZSUa', null), --jopass
(-5,'will', '$2a$10$gqg1kFIMowhVcpWMgd1.5ODh0pRIMkxuHmj7/KSR7cUpAcXRsWGf6', null), --wipass
(-6,'marean', '$2a$10$sKj2GWGGQrJtlFvdodW9supeyIO57xldi.nM373DDXfoqNSs3MewW', null); --mapass

insert into employees (id, role, name, working, account_id) values
(-1, 'Manager', 'Augustin', null, -1),
(-2, 'Waiter', 'Eusebiu', true, -2),
(-3, 'Waiter', 'Ioana', true, -3),
(-4, 'Waiter', 'John', false, -4),
(-5, 'Waiter', 'Will', false, -5),
(-6, 'Waiter', 'Marian', false, -6);

update users set employee_id = id where true;

insert into tables(id, unique_id/*, waiter_id*/) values
(-1, -1), /*-2*/
(-2, -2), /*-3*/
(-3, -3), /*-4*/
(-4, -4), /*-4*/
(-5, -5), /*-5*/
(-6, -6);

insert into menu_items(id, name, description, price, photo_path, available, category_id)
values (-1, 'Salsicciota', 'San Marzano Tomato Sauce, Buffalo Mozzarella, Mushroom, Sweet Italian Sausage, Thyme', 63, 'data/salsicciota.webp', true, -4),
       (-2, 'Marinara', 'San Marzano tomato sauce, garlic, oregano, extra virgin olive oil.',39, 'data/marinara.webp', true, -4),
       (-3, 'Trevisana', 'Braised Radicchio, Sauteed Red Onion, Bufala Mozzarella, Grana Padano DOP, Villa Monodori Aceto Balsamico', 46.5, 'data/trevisana.webp', true, -4),
       (-4, 'Zuccarella','Roasted Butternut Squash, Buffalo Mozzarella, Speck IGP, Sage', 50, 'data/zuccarella.webp',true, -4),
       (-5, 'Baby Greens Salad','Baby greens, toamtoes, carrots, walt, raisins and ctourons.',18, 'data/baby_greens_salad.webp', false, -3),
       (-6, 'Tartufella', 'Urbani White Truffle and Porcini Cream and Mozzarella di Bufala, finished with 3 grams of Fresh White Truffle',117, 'data/tartufella.webp', true, -4),
       (-7, 'Classic Bloody Mary', 'Our Vodka, homemade bloody mary mix.', 13, 'data/classic_bloody_mary.webp', true, -1),
       (-8, 'Cobb Salad', 'Roasted chicken, bacon, romaine, egg, avocado, tomato, and blue cheese.',20, 'data/cobb_salad.webp', true, -3),
       (-9, 'Honey Cake', 'Soft multilayer dulce de leche cake.', 15, 'data/honey_cake.webp', true, -5),
       (-10, 'Kale Salad','Raw kale, marinated red onions, dried cranberries, candied walnuts, blue cheese, pumpkin seeds and lemon olive oil',28, 'data/kale_salad.webp', false, -3),
       (-11, 'Hawaiian Pizza', 'Made of Ham,pineapple,mozzarella cheese,marinara sauce.',50, 'data/hawaiian_pizza.webp', true, -4),
       (-12, 'Pelamushi','Traditional Georgian sweet grape pudding.',20, 'data/pelamushi.webp', true, -5),
       (-13, 'Pink Lemonade','Pink Lemonade.',10, 'data/pink_lemonade.webp', true, -1),
       (-14, 'Grilled Salmon Salad','Cherry tomatoes, shaved fennel, baby spinach, arugula and fresh mint with balsamic lemon dill vinaigrette.',28, 'data/grilled_salmon_salad.webp', true, -3),
       (-15, 'Quattro Formaggi', 'Mozzarella, Gorgonzola Dolce, Parmigiano Reggiano DOP, Pecorino Romano.',51, 'data/quattro_formaggi.webp', false, -4),
       (-16, 'Morning Bacon Breakfast Burrito', 'Two scrambled eggs with crispy bacon, crispy potatoes, melted cheese, and caramelized onions wrapped up in a flour tortilla.', 10.8, 'data/morning_bacon_breakfast_burrito.webp', true, -2),
       (-17, 'Quinoa Avocado Salad','organic quinoa, fresh herbs, chickpeas, cucumber & tomato salad. House basil vinaigrette served on the side.',25.5, 'data/quinoa_avocado_salad.webp', true, -3),
       (-18, 'Capricciosa', 'San Marzano Tomato Sauce, Buffalo Mozzarella, Mushroom, Gaeta Black Olive, Rovagnati Granbiscotto Ham, Artichoke, Parmigiano Reggiano DOP',57, 'data/capricciosa.webp', true, -4),
       (-19, 'Sticky Toffee','Skillet baked, medjool dates, vanilla ice cream.', 22, 'data/sticky_toffee.webp', true, -5),
       (-20, 'Massese', 'San Marzano Tomato Sauce, Buffalo Mozzarella, Ferrarini Spicy Salami',60, 'data/massese.webp', true, -4),
       (-21, 'Warm Brownie and Ice Cream','Chocolate walnut brownie, choice of chocolate or vanilla ice cream.',12, 'data/warm_brownie_and_ice_cream.webp', true, -5),
       (-22, 'Westville Caesar', 'Crispy pancetta, avocado, fried halloumi cheese, alfalfa spouts, sunflower and pumpkin seeds over radicchio, endive, romaine & kale in lemon caesar dressing.', 32,'data/westville_caesar.webp' , true,-3),
       (-23, 'Super Mario Sandwich','Prosciutto Crudo di Parma, Salame, Mortadella, organic fresh mozzarella and E.V. olive oil.',18, 'data/super_mario_sandwich.webp', true, -2),
       (-24, 'Comfy Tomato and Spinach Breakfast Burrito','Two scrambled eggs with delicious breakfast sausage, crispy potatoes, melted cheese, tomatoes, and spinach wrapped up in a flour tortilla.',22.6, 'data/comfy_tomato_and_spinach_breakfast_burrito.webp', false, -2),
       (-25, 'Spinach Omelette','3 whole eggs mixed with customer choice of cheese and fresh spinach.',12.99, 'data/spinach_omelette.webp', true, -2),
       (-26, 'Iced Tea','Iced Tea.',5, 'data/iced_tea.webp', true, -1),
       (-27, 'Diet Coke','Diet Coke.',7, 'data/diet_coke.webp', true, -1),
       (-28, 'Variety Coffee','Variety Coffee.',5, 'data/variety_coffee.webp', true, -1),
       (-29, 'Michigan Sour Cherry','Tart, juicy, lattice crust. *Crust is made with leaf lard.',16, 'data/michigan_sour_cherry.webp', true, -5),
       (-30, 'Black and Creme','Jameson, variety drip coffee, cocoa, frangelico, amaro, cream, nutmeg. Must be 21 to purchase.',25, 'data/black_and_creme.webp', true, -1);

insert into users_authorities (user_id, authority_id)
values (-1, -1),
       (-1, -2),
       (-2, -2),
       (-3, -2),
       (-4, -2),
       (-5, -2),
       (-6, -2);

insert into orders (id, comments, done, order_date, table_id, waiter_id)
values (-1, '-', true, '2020-6-1 18:23:00', -3, -6),
       (-2, '-', true, '2020-4-26 18:23:00', -5, -3),
       (-3, '-', false, '2020-9-12 18:23:00', -4, -5),
       (-4, '-', true, '2020-7-24 18:23:00', -5, -2),
       (-5, '-', true, '2020-1-3 18:23:00', -2, -5),
       (-6, '-', true, '2020-9-11 18:23:00', -4, -6),
       (-7, 'comment_1', true, '2020-9-17 18:23:00', -4, -5),
       (-8, '-', true, '2020-7-2 18:23:00', -4, -5),
       (-9, '-', true, '2020-2-13 18:23:00', -4, -6),
       (-10, '-', false, '2020-8-18 18:23:00', -5, -5),
       (-11, '-', true, '2020-11-18 18:23:00', -3, -3),
       (-12, 'comment_4', true, '2020-2-12 18:23:00', -2, -3),
       (-13, '-', false, '2020-9-8 18:23:00', -3, -5),
       (-14, '-', true, '2020-3-1 18:23:00', -2, -3),
       (-15, '-', true, '2020-9-11 18:23:00', -1, -4),
       (-16, '-', true, '2020-10-14 18:23:00', -3, -6),
       (-17, '-', true, '2020-9-20 18:23:00', -2, -6),
       (-18, 'comment_2', true, '2020-2-4 18:23:00', -1, -4),
       (-19, '-', true, '2020-10-2 18:23:00', -1, -2),
       (-20, '-', true, '2020-3-26 18:23:00', -4, -3),
       (-21, '-', true, '2020-5-28 18:23:00', -3, -3),
       (-22, '-', true, '2020-3-19 18:23:00', -3, -3),
       (-23, 'comment_3', false, '2020-1-20 18:23:00', -4, -4),
       (-24, '-', true, '2020-10-3 18:23:00', -3, -2),
       (-25, '-', true, '2020-12-10 18:23:00', -5, -4);

insert into orders_menu_items (order_id, menu_item_id) values
(-5, -18),
(-8, -9),
(-17, -6),
(-1, -13),
(-9, -2),
(-25, -1),
(-14, -10),
(-19, -23),
(-4, -17),
(-18, -1),
(-10, -24),
(-16, -2),
(-21, -2),
(-6, -24),
(-13, -4),
(-2, -7),
(-3, -14),
(-11, -21),
(-1, -7),
(-12, -12),
(-20, -20),
(-17, -21),
(-22, -2),
(-15, -18),
(-7, -5),
(-24, -15),
(-3, -22),
(-8, -18),
(-23, -8);
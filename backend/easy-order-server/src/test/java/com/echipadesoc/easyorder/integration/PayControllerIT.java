package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.dtos.CreditCardDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PayControllerIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;

    @Value("${spring.rest.pay-path}")
    private String payPath;

    private RestTemplate restTemplate;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
    }

    @Test
    public void pay_OK() {
        CreditCardDTO creditCard = new CreditCardDTO("aaa aa", "1234", "05/22", 510);
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(payPath)
                .build()
                .toUri();

        HttpEntity<CreditCardDTO> requestEntity = new HttpEntity<>(creditCard);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity,
                                                                    new ParameterizedTypeReference<>() {});
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void pay_InsufficientFund() {
        CreditCardDTO creditCard = new CreditCardDTO("aaa aa", "1234", "05/22", 110);
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(payPath)
                .build()
                .toUri();

        HttpEntity<CreditCardDTO> requestEntity = new HttpEntity<>(creditCard);
        try {
            ResponseEntity<Void> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity,
                                                                        new ParameterizedTypeReference<>() {});

            assertEquals(responseEntity.getStatusCode(), HttpStatus.PAYMENT_REQUIRED);
        } catch (HttpClientErrorException e) {
            assertEquals(402, e.getRawStatusCode());
        }
    }

    @Test
    public void pay_InvalidCardData() {
        CreditCardDTO creditCard = new CreditCardDTO("aaa aa", null, "05/22", 110);
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(payPath)
                .build()
                .toUri();

        HttpEntity<CreditCardDTO> requestEntity = new HttpEntity<>(creditCard);
        try {
            ResponseEntity<Void> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity,
                                                                        new ParameterizedTypeReference<>() {});

            assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
        } catch (HttpClientErrorException e) {
            assertEquals(406, e.getRawStatusCode());
        }
    }
}

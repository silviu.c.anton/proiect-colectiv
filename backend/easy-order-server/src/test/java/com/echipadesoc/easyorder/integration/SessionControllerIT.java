package com.echipadesoc.easyorder.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SessionControllerIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;

    @Value("${spring.rest.session-path}")
    private String sessionPath;

    @Value("${spring.rest.newSessionId-path}")
    private String newSessionIdPath;

    private RestTemplate restTemplate;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
    }

    @Test
    public void getNewSessionToken_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(sessionPath)
                .pathSegment(newSessionIdPath)
                .build()
                .toUri();
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        String response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.length() >= 10);
    }
}

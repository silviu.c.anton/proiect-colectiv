package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.dtos.LoginDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class TestUtils {

    private final RestTemplate restTemplate = new RestTemplate();

    private final String baseUrl;

    private final String loginPath;

    public TestUtils(String baseUrl, String loginPath) {
        this.baseUrl = baseUrl;
        this.loginPath = loginPath;
    }

    public MultiValueMap<String, String> getAccessHeadersManager() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(loginPath)
                .build()
                .toUri();

        HttpEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST,
                new HttpEntity<>(new LoginDTO("manager", "manager")), String.class);

        String token = response.getHeaders().getFirst("accessToken");
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.add("Authorization", token);
        return headers;
    }
    public MultiValueMap<String, String> getAccessHeadersWaiter() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(loginPath)
                .build()
                .toUri();

        HttpEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST,
                new HttpEntity<>(new LoginDTO("waiter", "waiter")), String.class);

        String token = response.getHeaders().getFirst("accessToken");
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.add("Authorization", token);
        return headers;
    }
}

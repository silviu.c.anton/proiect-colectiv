package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.OrderNotFoundException;
import com.echipadesoc.easyorder.domain.model.Category;
import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.Order;
import com.echipadesoc.easyorder.domain.model.Table;
import com.echipadesoc.easyorder.domain.model.Waiter;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayOrderDTO;
import com.echipadesoc.easyorder.domain.utils.DtoMapper;
import com.echipadesoc.easyorder.repositories.CategoryRepository;
import com.echipadesoc.easyorder.repositories.MenuItemRepository;
import com.echipadesoc.easyorder.repositories.OrderRepository;
import com.echipadesoc.easyorder.repositories.TableRepository;
import com.echipadesoc.easyorder.repositories.WaiterRepository;
import com.echipadesoc.easyorder.domain.exceptions.SameOrderStatusException;
import com.echipadesoc.easyorder.domain.exceptions.WaiterNotFoundException;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
    @Mock
    OrderRepository orderRepository;
    @Mock
    MenuItemRepository menuItemRepository;
    @Mock
    TableRepository tableRepository;
    @Mock
    WaiterRepository waiterRepository;
    @Mock
    CategoryRepository categoryRepository;
    @Mock
    DtoMapper dtoMapper;

    private OrderService orderService;
    private Order order;
    private List<MenuItem> itemList;
    private MenuItem menuItem;
    private Category category;
    private Long menuItemId;
    private Long orderId;
    private Waiter waiter;
    private List<Order> orderList;
    private Long waiterId;

    @Before
    public void init() {
        initList();

        this.orderService = new OrderServiceImpl(orderRepository, menuItemRepository, tableRepository, waiterRepository,
                dtoMapper);
    }

    private void initList() {
        this.itemList = new ArrayList<>();
        long id = 12345678;
        String name = "name";
        String description = "description";
        float price = 0.01f;
        String photoPath = "photoPath";
        byte[] photo = null;
        boolean available = true;
        Category category = new Category(123456, "A_MY_CATEGORY");
        this.category = new Category(0, "CategoryTest");
        this.menuItemId = 1L;
        this.menuItem = new MenuItem(id, name, description, price, photoPath, available, category);
        this.itemList.add(menuItem);
        this.itemList.add(new MenuItem(11111111L, "aaaa", "description1", 0.01f,
                null, true, new Category(111111L, "categoryName1")));
        this.itemList.add(new MenuItem(22222222L, "kkkk", "description2", 0.02f,
                null, true, new Category(222222L, "categoryName2")));
        this.orderId = 1L;
        this.waiter = new Waiter();
        this.waiterId = 1L;
        this.waiter.setId(waiterId);
        this.order = new Order(this.orderId, "comments", LocalDateTime.now(), false,
                new Table(1L, 1), this.waiter, itemList);
        this.orderList = new ArrayList<>();
        orderList.add(order);
    }

    @Test
    public void changeOrderStatusTest() {
        when(orderRepository.findById(order.getId())).thenReturn(Optional.ofNullable(order));
        when(orderRepository.save(order)).thenReturn(order);
        orderService.updateOrderStatus(orderId,true);
        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, times(1)).save(order);
    }
    @Test
    public void changeOrderStatus_OrderNotFoundTest() {
        when(orderRepository.findById(order.getId())).thenReturn(Optional.empty());
        assertThrows(OrderNotFoundException.class, () -> orderService.updateOrderStatus(orderId,false));
        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, times(0)).save(order);
    }
    @Test
    public void changeOrderStatus_OrderAlreadyDoneTest() {
        this.order.setDone(true);
        when(orderRepository.findById(order.getId())).thenReturn(Optional.ofNullable(order));
        assertThrows(SameOrderStatusException.class, () -> orderService.updateOrderStatus(orderId,true));
        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, times(0)).save(order);
    }
    @Test
    public void getOrdersByWaiterIdWithStatusTest_HasOrders(){
        when(orderRepository.findAll()).thenReturn(this.orderList);
        when(waiterRepository.existsById(this.waiterId)).thenReturn(true);
        List<DisplayOrderDTO> orders =  this.orderService.getOrdersByWaiterIdWithStatus(this.waiterId, false);
        assertEquals(orders.size(), 1);
        verify(waiterRepository, times(1)).existsById(this.waiterId);
        verify(orderRepository, times(1)).findAll();
    }
    @Test
    public void getOrdersByWaiterIdWithStatusTest_HasNoOrders() {
        when(orderRepository.findAll()).thenReturn(this.orderList);
        when(waiterRepository.existsById(this.waiterId)).thenReturn(true);
        List<DisplayOrderDTO> orders =  this.orderService.getOrdersByWaiterIdWithStatus(this.waiterId, true);
        assertEquals(orders.size(), 0);
        verify(waiterRepository, times(1)).existsById(this.waiterId);
        verify(orderRepository, times(1)).findAll();
    }
    @Test
    public void getOrdersByWaiterIdWithStatusTest_WaiterNotFound(){
        when(waiterRepository.existsById(this.waiterId)).thenReturn(false);
        assertThrows(WaiterNotFoundException.class, () ->this.orderService.getOrdersByWaiterIdWithStatus(this.waiterId, false));
        verify(waiterRepository, times(1)).existsById(this.waiterId);
        verify(orderRepository, times(0)).findAll();
    }
}

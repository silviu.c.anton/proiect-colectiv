package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.dtos.DisplayOrderDTO;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderControllerIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;

    @Value("${spring.rest.order-path}")
    private String orderPath;

    private RestTemplate restTemplate;
    private OrderDTO okOrderDTO;
    private OrderDTO noTableFoundOrderDTO;
    private OrderDTO noMenuItemFoundOrderDTO;
    @Value("${spring.rest.login-path}")
    private String loginPath;

    private TestUtils testUtils;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
        initOrders();
        this.testUtils = new TestUtils(baseUrl, loginPath);
    }

    private void initOrders() {
        okOrderDTO = new OrderDTO();
        okOrderDTO.setComments("burger without onion");
        okOrderDTO.setTableId(-1L);
        okOrderDTO.setMenuItemIds(Arrays.asList(-4L, -2L, -1L));

        noTableFoundOrderDTO = new OrderDTO();
        noTableFoundOrderDTO.setComments("burger without onion");
        noTableFoundOrderDTO.setTableId(-100L);
        noTableFoundOrderDTO.setMenuItemIds(Arrays.asList(-4L, -2L, -1L));

        noMenuItemFoundOrderDTO = new OrderDTO();
        noMenuItemFoundOrderDTO.setComments("burger without onion");
        noMenuItemFoundOrderDTO.setTableId(-1L);
        noMenuItemFoundOrderDTO.setMenuItemIds(Arrays.asList(-400L, -200L, -100L));
    }

    @Test
    public void saveOrder_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .build()
                .toUri();
        RequestEntity<OrderDTO> requestEntity = new RequestEntity<>(okOrderDTO, HttpMethod.POST, uri);
        ResponseEntity<DisplayOrderDTO> responseEntity = restTemplate.exchange(requestEntity, DisplayOrderDTO.class);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals(responseEntity.getBody().getTableId(), Long.valueOf(-1));
    }

    @Test
    public void saveOrder_NOT_FOUND() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .build()
                .toUri();
        try {
            RequestEntity<OrderDTO> requestEntity = new RequestEntity<>(noTableFoundOrderDTO, HttpMethod.POST, uri);
            restTemplate.exchange(requestEntity, OrderDTO.class);
            fail("Should have gotten 404 NOT_FOUND");
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void markOrderAsDone_Ok() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .pathSegment("-10")
                .pathSegment("markAsDone")
                .build()
                .toUri();

        HttpEntity<String> requestUpdate = new HttpEntity<>("body", testUtils.getAccessHeadersWaiter());

        ResponseEntity<String> responseEntity =
                restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void markOrderAsNotDone_Ok() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .pathSegment("-10")
                .pathSegment("markAsNotDone")
                .build()
                .toUri();

        HttpEntity<String> requestUpdate = new HttpEntity<>("body", testUtils.getAccessHeadersManager());

        ResponseEntity<String> responseEntity =
                restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void markOrderAsDone_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .pathSegment("-4")
                .pathSegment("markAsDone")
                .build()
                .toUri();

        HttpEntity<String> requestUpdate = new HttpEntity<>("body");

        try {
            restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {});
            fail("Should have gotten 403 FORBIDDEN");
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void markOrderAsDone_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .pathSegment("-200")
                .pathSegment("markAsDone")
                .build()
                .toUri();

        HttpEntity<String> requestUpdate = new HttpEntity<>("body", testUtils.getAccessHeadersWaiter());

        try {
            restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {});
            fail("Should have gotten 404 NOT_FOUND");
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void saveOrder_NOT_FOUND_ITEM_IN_DB() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .build()
                .toUri();
        try {
            RequestEntity<OrderDTO> requestEntity = new RequestEntity<>(noMenuItemFoundOrderDTO, HttpMethod.POST, uri);
            restTemplate.exchange(requestEntity, OrderDTO.class);
            fail("Should have gotten 404 NOT_FOUND");
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    public void markOrderAsDone_AlreadyDone() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .pathSegment("-1")
                .pathSegment("markAsDone")
                .build()
                .toUri();

        HttpEntity<String> requestUpdate = new HttpEntity<>("body", testUtils.getAccessHeadersWaiter());

        try {
            restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {});
            fail("Should have gotten 409 CONFLICT");
        } catch (HttpClientErrorException e) {
            assertEquals(409, e.getRawStatusCode());
        }
    }

    @Test
    public void getOrdersForWaiterWithStatus_Ok() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .queryParam("id", "-4")
                .build()
                .toUri();
        HttpEntity<List<OrderDTO>> requestGetOrdersForWaiterWithStatus = new HttpEntity<>(null, testUtils.getAccessHeadersWaiter());
        ResponseEntity<List<OrderDTO>> responseEntity =
                restTemplate.exchange(uri, HttpMethod.GET, requestGetOrdersForWaiterWithStatus, new ParameterizedTypeReference<>() {
                });
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
    }

    @Test
    public void getOrdersForWaiterWithStatus_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .queryParam("id", "100")
                .build()
                .toUri();
        HttpEntity<List<OrderDTO>> requestGetOrdersForWaiterWithStatus = new HttpEntity<>(null, testUtils.getAccessHeadersWaiter());
        try {
            restTemplate.exchange(uri, HttpMethod.GET, requestGetOrdersForWaiterWithStatus, new ParameterizedTypeReference<>() {});
            fail("Should have gotten 404 NOT_FOUND");
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void getOrdersForWaiterWithStatus_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(orderPath)
                .queryParam("id", "-1")
                .build()
                .toUri();
        HttpEntity<List<OrderDTO>> requestGetOrdersForWaiterWithStatus = new HttpEntity<>(null);
        try {
            restTemplate.exchange(uri, HttpMethod.GET, requestGetOrdersForWaiterWithStatus, new ParameterizedTypeReference<>() {});
            fail("Should have gotten 403 FORBIDDEN");
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }
}

package com.echipadesoc.easyorder.services;

import com.echipadesoc.easyorder.domain.exceptions.MenuItemNotFoundException;
import com.echipadesoc.easyorder.domain.exceptions.MenuItemUsedByOrderException;
import com.echipadesoc.easyorder.domain.model.Category;
import com.echipadesoc.easyorder.domain.model.MenuItem;
import com.echipadesoc.easyorder.domain.model.dtos.DisplayMenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuSectionDTO;
import com.echipadesoc.easyorder.domain.utils.DtoMapper;
import com.echipadesoc.easyorder.repositories.CategoryRepository;
import com.echipadesoc.easyorder.repositories.MenuItemRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.doThrow;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class MenuServiceTest {

    @Mock
    MenuItemRepository menuItemRepository;

    @Mock
    DtoMapper dtoMapper;

    @Mock
    private CategoryRepository categoryRepository;
    private MenuServiceImpl menuService;
    private List<MenuItem> itemList;
    private MenuItem menuItem;
    private MenuItemDTO menuItemDTO;
    private DisplayMenuItemDTO displayMenuItemDTO;
    private Category category;
    private Long menuItemId;

    @Before
    public void init() {
        initList();
        this.menuService = new MenuServiceImpl(menuItemRepository, categoryRepository, dtoMapper);
    }

    private void initList() {
        this.itemList = new ArrayList<>();
        long id = 12345678;
        String name = "name";
        String description = "description";
        float price = 0.01f;
        String photoPath = "photoPath";
        boolean available = true;
        Category category = new Category(123456, "A_MY_CATEGORY");
        this.category = new Category(0, "CategoryTest");
        this.menuItemId = 1L;
        this.menuItem = new MenuItem(id, name, description, price, photoPath, available, category);
        this.menuItemDTO = new MenuItemDTO(id, name, description, price, photoPath, null, available, category.getName());
        this.displayMenuItemDTO = new DisplayMenuItemDTO(id, name, description, price, null, available);
        this.itemList.add(menuItem);
        this.itemList.add(new MenuItem(11111111L, "aaaa", "description1", 0.01f,
                                       null, true, new Category(111111L, "categoryName1")));
        this.itemList.add(new MenuItem(22222222L, "kkkk", "description2", 0.02f,
                                       null, true, new Category(222222L, "categoryName2")));
        this.itemList.add(new MenuItem(33333333L, "wwww", "description3", 0.03f,
                                       null, true, new Category(333333L, "categoryName3")));
        this.itemList.add(new MenuItem(44444444L, "eeee", "description4", 0.04f,
                                       null, true, new Category(444444L, "categoryName4")));
    }

    @Test
    public void getMenuGroupedByCategoryNameTest() {
        when(dtoMapper.menuItemToDisplayMenuItemDto(menuItem)).thenReturn(displayMenuItemDTO);
        when(menuItemRepository.getAllByCategoryNotNull()).thenReturn(itemList);
        List<MenuSectionDTO> groupedItems = menuService.getMenu();
        verify(dtoMapper, times(1)).menuItemToDisplayMenuItemDto(menuItem);
        verify(menuItemRepository, times(1)).getAllByCategoryNotNull();
        assertEquals(5, groupedItems.size());
        groupedItems.sort(Comparator.comparing(section -> section.getCategoryName().toUpperCase()));
        assertEquals("A_MY_CATEGORY", groupedItems.get(0).getCategoryName());
        assertEquals(1, groupedItems.get(0).getMenuItems().size());
        assertEquals(groupedItems.get(0).getMenuItems().get(0), displayMenuItemDTO);
    }

    @Test
    public void updateMenuItemTest_CategoryExists() {
        when(dtoMapper.menuItemToMenuItemDto(menuItem)).thenReturn(menuItemDTO);
        when(menuItemRepository.findById(menuItem.getId())).thenReturn(Optional.ofNullable(menuItem));
        when(menuItemRepository.save(menuItem)).thenReturn(menuItem);
        when(categoryRepository.findByName(menuItem.getCategory().getName())).thenReturn(Optional.of(menuItem.getCategory()));
        MenuItemDTO result = menuService.updateMenuItem(menuItemDTO);
        verify(dtoMapper, times(1)).menuItemToMenuItemDto(menuItem);
        verify(categoryRepository).findByName(menuItem.getCategory().getName());
        verify(menuItemRepository).save(menuItem);
        assertEquals(menuItemDTO, result);
    }

    @Test
    public void updateMenuItemTest_CategoryDoesNotExist() {
        menuItem.setCategory(category);
        menuItemDTO.setCategory(category.getName());
        when(dtoMapper.menuItemToMenuItemDto(menuItem)).thenReturn(menuItemDTO);
        when(menuItemRepository.findById(menuItem.getId())).thenReturn(Optional.ofNullable(menuItem));
        when(menuItemRepository.save(menuItem)).thenReturn(menuItem);
        when(categoryRepository.save(category)).thenReturn(category);
        when(categoryRepository.findByName(category.getName())).thenReturn(Optional.empty());
        MenuItemDTO result = menuService.updateMenuItem(menuItemDTO);
        verify(dtoMapper, times(1)).menuItemToMenuItemDto(menuItem);
        verify(categoryRepository).findByName(category.getName());
        verify(categoryRepository).save(category);
        verify(menuItemRepository).save(menuItem);
        assertEquals(menuItemDTO, result);
    }

    @Test
    public void addMenuItemTest_CategoryExists() {
        MenuItem mappedMenuItem = new MenuItem(0L, menuItem.getName(), menuItem.getDescription(), menuItem.getPrice(), menuItem.getPhotoPath(), menuItem.isAvailable(), menuItem.getCategory());
        when(menuItemRepository.save(mappedMenuItem)).thenReturn(menuItem);
        when(categoryRepository.findByName(menuItem.getCategory().getName())).thenReturn(Optional.of(menuItem.getCategory()));
        when(dtoMapper.menuItemToMenuItemDto(menuItem)).thenReturn(menuItemDTO);
        MenuItemDTO result = menuService.addMenuItem(menuItemDTO);
        verify(categoryRepository).findByName(menuItem.getCategory().getName());
        verify(menuItemRepository).save(mappedMenuItem);
        verify(dtoMapper, times(1)).menuItemToMenuItemDto(menuItem);
        assertEquals(menuItemDTO, result);
    }

    @Test
    public void addMenuItemTest_CategoryDoesNotExist() {
        menuItem.setCategory(category);
        menuItemDTO.setCategory(category.getName());
        MenuItem mappedMenuItem = new MenuItem(0L, menuItem.getName(), menuItem.getDescription(), menuItem.getPrice(), menuItem.getPhotoPath(), menuItem.isAvailable(), menuItem.getCategory());
        when(menuItemRepository.save(mappedMenuItem)).thenReturn(menuItem);
        when(categoryRepository.save(category)).thenReturn(category);
        when(categoryRepository.findByName(category.getName())).thenReturn(Optional.empty());
        when(dtoMapper.menuItemToMenuItemDto(menuItem)).thenReturn(menuItemDTO);
        MenuItemDTO result = menuService.addMenuItem(menuItemDTO);
        verify(categoryRepository).findByName(category.getName());
        verify(categoryRepository).save(category);
        verify(menuItemRepository).save(mappedMenuItem);
        verify(dtoMapper, times(1)).menuItemToMenuItemDto(menuItem);
        assertEquals(menuItemDTO, result);
    }

    @Test
    public void getSorting() {
        String sortBy1 = "name.asc";
        Sort.Order order1 = new Sort.Order(Sort.Direction.ASC, "name");
        List<Sort.Order> orders = menuService.getSorting(sortBy1);
        assertEquals(order1, orders.get(0));
        String sortBy2 = "price.desc";
        String sortBy = sortBy1 + "," + sortBy2;
        Sort.Order order2 = new Sort.Order(Sort.Direction.DESC, "price");
        orders.clear();
        orders = menuService.getSorting(sortBy);
        List<Sort.Order> testOrders = new ArrayList<>();
        testOrders.add(order1);
        testOrders.add(order2);
        for (int i = 0; i < 2; i++) {
            assertEquals(testOrders.get(i), orders.get(i));
        }
    }

    @Test
    public void getMenuItemsPagedAndSorted() {
        String sortBy = "name.desc,price.asc";
        List<Sort.Order> sortOrders = menuService.getSorting(sortBy);
        Pageable pageRequest = PageRequest.of(0, 3, Sort.by(sortOrders));
        Page<MenuItem> itemPage = getItemPage(pageRequest);

        assertEquals(itemPage.getTotalPages(), 2);
        when(menuItemRepository.findAll(ArgumentMatchers.isA(Pageable.class))).thenReturn(itemPage);
        List<MenuItemDTO> menuItems = menuService.getMenuItems(0, 3, sortBy);

        verify(menuItemRepository).findAll(PageRequest.of(0, 3, Sort.by(sortOrders)));
    }

    private Page<MenuItem> getItemPage(Pageable pageRequest) {
        int start = (int) pageRequest.getOffset();
        int end = Math.min(start + pageRequest.getPageSize(), itemList.size());
        return new PageImpl<>(itemList.subList(start, end), pageRequest, itemList.size());
    }

    @Test
    public void getMenuItemsPaged() {
        Pageable pageRequest = PageRequest.of(1, 3);
        Page<MenuItem> itemPage = getItemPage(pageRequest);
        when(menuItemRepository.findAll(ArgumentMatchers.isA(PageRequest.class))).thenReturn(itemPage);
        List<MenuItemDTO> menuItems = menuService.getMenuItems(1, 3, null);

        verify(menuItemRepository).findAll(PageRequest.of(1, 3));
    }

    @Test
    public void getMenuItemsSorted() {
        String sortBy = "name.desc";
        List<Sort.Order> sortOrders = menuService.getSorting(sortBy);
        Pageable pageRequest = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(sortOrders));
        Page<MenuItem> itemPage = getItemPage(pageRequest);

        when(menuItemRepository.findAll(ArgumentMatchers.isA(Pageable.class))).thenReturn(itemPage);
        List<MenuItemDTO> menuItems = menuService.getMenuItems(null, null, sortBy);
        verify(menuItemRepository).findAll(pageRequest);
    }

    @Test
    public void getMenuItemsNormal() {
        Pageable pageRequest = PageRequest.of(0, Integer.MAX_VALUE);
        Page<MenuItem> itemPage = getItemPage(pageRequest);
        when(menuItemRepository.findAll(ArgumentMatchers.isA(Pageable.class))).thenReturn(itemPage);
        List<MenuItemDTO> menuItems = menuService.getMenuItems(null, null, null);
        verify(menuItemRepository).findAll(pageRequest);
    }

    @Test
    public void getMenuItemById() {
        menuItem.setId(menuItemId);
        menuItemDTO.setId(menuItemId);
        when(menuItemRepository.findById(menuItemId)).thenReturn(Optional.ofNullable(menuItem));
        when(dtoMapper.menuItemToMenuItemDto(menuItem)).thenReturn(menuItemDTO);
        MenuItemDTO menuItemDTO = menuService.getMenuItemById(menuItemId);
        verify(menuItemRepository).findById(menuItemId);
        verify(dtoMapper).menuItemToMenuItemDto(menuItem);
        assertEquals(Optional.of(menuItemDTO.getId()).get(), menuItemId);
    }

    @Test
    public void getMenuItemById_NotFound() {
        menuItem.setId(menuItemId);
        when(menuItemRepository.findById(menuItemId)).thenReturn(Optional.empty());
        assertThrows(MenuItemNotFoundException.class, () -> menuService.getMenuItemById(menuItemId));
        verify(menuItemRepository).findById(menuItemId);
    }

    @Test
    public void deleteMenuItem() {
        menuItem.setId(menuItemId);
        when(menuItemRepository.existsById(menuItemId)).thenReturn(true).thenReturn(false);
        menuService.deleteMenuItem(menuItemId);
        verify(menuItemRepository, times(1)).existsById(menuItemId);
        verify(menuItemRepository, times(1)).deleteById(menuItemId);
        assertThrows(MenuItemNotFoundException.class, () -> menuService.deleteMenuItem(menuItemId));
    }

    @Test
    public void deleteMenuItem_NotFound() {
        menuItem.setId(menuItemId);
        when(menuItemRepository.existsById(menuItemId)).thenReturn(false);
        assertThrows(MenuItemNotFoundException.class, () -> menuService.deleteMenuItem(menuItemId));
        verify(menuItemRepository, times(1)).existsById(menuItemId);
        verify(menuItemRepository, times(0)).deleteById(menuItemId);
    }

    @Test
    public void deleteMenuItem_UsedByOrder() {
        menuItem.setId(menuItemId);
        when(menuItemRepository.existsById(menuItemId)).thenReturn(true);
        doThrow(MenuItemUsedByOrderException.class).when(menuItemRepository).deleteById(menuItemId);
        assertThrows(MenuItemUsedByOrderException.class, () -> menuService.deleteMenuItem(menuItemId));
        verify(menuItemRepository, times(1)).existsById(menuItemId);
        verify(menuItemRepository, times(1)).deleteById(menuItemId);
    }

}

package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.dtos.CreditCardDTO;
import com.echipadesoc.easyorder.domain.model.dtos.OrderDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WaiterControllerTestIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;

    @Value("${spring.rest.waiter-path}")
    private String waiterApiPath;

    @Value("${spring.rest.login-path}")
    private String loginPath;

    private TestUtils testUtils;

    private RestTemplate restTemplate;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
        this.testUtils = new TestUtils(baseUrl, loginPath);
    }

    @Test
    public void getWaiterIdByUsername_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(waiterApiPath)
                .path("idByUsername")
                .queryParam("username", "waiter")
                .build()
                .toUri();
        RequestEntity<String> requestEntity =
                new RequestEntity<>(testUtils.getAccessHeadersWaiter(), HttpMethod.GET, uri);
        ResponseEntity<Long> responseEntity = restTemplate.exchange(requestEntity, Long.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(Long.valueOf(-2L), responseEntity.getBody());
    }

    @Test
    public void getWaiterIdByUsername_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(waiterApiPath)
                .path("idByUsername")
                .queryParam("username", "asdfgjk")
                .build()
                .toUri();
        RequestEntity<String> requestEntity =
                new RequestEntity<>(testUtils.getAccessHeadersWaiter(), HttpMethod.GET, uri);
        try {
            restTemplate.exchange(requestEntity, Long.class);
            fail("Should have gotten 404 NOT_FOUND");
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
            assertNotNull(e.getResponseBodyAsString());
        }
    }

    @Test
    public void getWaiterIdByUsername_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(waiterApiPath)
                .path("idByUsername")
                .queryParam("username", "asdfgjk")
                .build()
                .toUri();
        RequestEntity<String> requestEntity = new RequestEntity<>("", HttpMethod.GET, uri);
        try {
            restTemplate.exchange(requestEntity, Long.class);
            Assert.fail("Should have gotten 403 FORBIDDEN");
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

}

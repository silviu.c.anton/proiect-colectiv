package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.TempOrder;
import com.echipadesoc.easyorder.domain.model.dtos.TempOrderDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TempOrderControllerIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;


    @Value("${spring.rest.tempOrders-path}")
    private String tempOrdersPath;

    private TempOrderDTO tempOrderDTOOkFirst;
    private TempOrderDTO tempOrderDTOOkSecond;
    private TempOrderDTO tempOrderDTOConflict;

    private String tempOrderOkSession = "SESSION:-3";
    private String tempOrderConflictSession = "SESSION:-1";

    private RestTemplate restTemplate;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
        initTempOrders();
    }

    @Test
    public void store_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .build()
                .toUri();

        RequestEntity<TempOrderDTO> requestEntity = new RequestEntity<>(tempOrderDTOOkFirst, HttpMethod.POST, uri);
        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(responseEntity.getBody());

        URI cleanUpUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        RequestEntity<String> cleanupRequestEntity = new RequestEntity<>(null, HttpMethod.DELETE, cleanUpUri);
        restTemplate.exchange(cleanupRequestEntity, Void.class);
    }

    @Test
    public void store_CONFLICT() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .build()
                .toUri();

        RequestEntity<TempOrderDTO> requestEntity = new RequestEntity<>(tempOrderDTOConflict, HttpMethod.POST, uri);
        restTemplate.exchange(requestEntity, String.class);
        try {
            restTemplate.exchange(requestEntity, String.class);
            fail("Should have gotten 409 CONFLICT");
        } catch (HttpClientErrorException e) {
            assertEquals(409, e.getRawStatusCode());
            assertNotNull(e.getResponseBodyAsString());

            URI cleanUpUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                    .pathSegment(tempOrdersPath)
                    .pathSegment(tempOrderConflictSession)
                    .build()
                    .toUri();

            RequestEntity<String> cleanupRequestEntity = new RequestEntity<>(null, HttpMethod.DELETE, cleanUpUri);
            restTemplate.exchange(cleanupRequestEntity, Void.class);
        }
    }

    @Test
    public void store_SECOND_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .build()
                .toUri();

        RequestEntity<TempOrderDTO> requestEntity = new RequestEntity<>(tempOrderDTOOkFirst, HttpMethod.POST, uri);
        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(responseEntity.getBody());

        requestEntity = new RequestEntity<>(tempOrderDTOOkSecond, HttpMethod.POST, uri);
        responseEntity = restTemplate.exchange(requestEntity, String.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(responseEntity.getBody());

        URI cleanUpUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        RequestEntity<String> cleanupRequestEntity = new RequestEntity<>(null, HttpMethod.DELETE, cleanUpUri);
        restTemplate.exchange(cleanupRequestEntity, Void.class);
    }



    private void initTempOrders() {
        tempOrderDTOOkFirst = new TempOrderDTO();
        tempOrderDTOOkFirst.setId(-1L);
        tempOrderDTOOkFirst.setMenuItemIds(new ArrayList<>());
        tempOrderDTOOkFirst.setNickname("Ion");
        tempOrderDTOOkFirst.setSessionId(tempOrderOkSession);

        tempOrderDTOOkSecond = new TempOrderDTO();
        tempOrderDTOOkSecond.setId(-2L);
        tempOrderDTOOkSecond.setMenuItemIds(new ArrayList<>());
        tempOrderDTOOkSecond.setNickname("Vasile");
        tempOrderDTOOkSecond.setSessionId(tempOrderOkSession);

        tempOrderDTOConflict = new TempOrderDTO();
        tempOrderDTOConflict.setId(-3L);
        tempOrderDTOConflict.setMenuItemIds(new ArrayList<>());
        tempOrderDTOConflict.setNickname("Ion");
        tempOrderDTOConflict.setSessionId(tempOrderConflictSession);
    }

    @Test
    public void update_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .build()
                .toUri();

        RequestEntity<TempOrderDTO> requestEntity = new RequestEntity<>(tempOrderDTOOkFirst, HttpMethod.POST, uri);
        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(responseEntity.getBody());

        tempOrderDTOOkFirst.setMenuItemIds(Arrays.asList(-1L, -1L, -1L));

        RequestEntity<TempOrderDTO> requestEntityUpdate = new RequestEntity<>(tempOrderDTOOkFirst, HttpMethod.PUT, uri);
        ResponseEntity<String> responseEntityUpdate = restTemplate.exchange(requestEntityUpdate, String.class);
        assertEquals(responseEntityUpdate.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntityUpdate.getBody());

        URI cleanUpUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        RequestEntity<String> cleanupRequestEntity = new RequestEntity<>(null, HttpMethod.DELETE, cleanUpUri);
        restTemplate.exchange(cleanupRequestEntity, Void.class);
    }

    @Test
    public void delete_empty_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        RequestEntity<String> requestEntity = new RequestEntity<>(null, HttpMethod.DELETE, uri);
        ResponseEntity<Void> responseEntity = restTemplate.exchange(requestEntity, Void.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNull(responseEntity.getBody());
    }

    @Test
    public void add_delete_OK() {
        // add tempOrderOkFirst
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .build()
                .toUri();

        RequestEntity<TempOrderDTO> requestEntity = new RequestEntity<>(tempOrderDTOOkFirst, HttpMethod.POST, uri);
        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(responseEntity.getBody());

        // test if tempOrderOkFirst added
        URI getUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        ResponseEntity<List<TempOrder>> getResponseEntity =
                restTemplate.exchange(getUri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {});

        assertEquals(getResponseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(getResponseEntity.getBody());
        assertEquals(1, getResponseEntity.getBody().size());
        assertEquals(tempOrderDTOOkFirst.getNickname(), getResponseEntity.getBody().get(0).getNickname());
        assertEquals(tempOrderDTOOkFirst.getSessionId(), getResponseEntity.getBody().get(0).getSessionId());

        // delete all tempOrders having tempOrderOkSession
        URI cleanUpUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(tempOrdersPath)
                .pathSegment(tempOrderOkSession)
                .build()
                .toUri();

        RequestEntity<String> cleanUpRequestEntity = new RequestEntity<>(null, HttpMethod.DELETE, cleanUpUri);
        ResponseEntity<Void> cleanUpResponseEntity = restTemplate.exchange(cleanUpRequestEntity, Void.class);
        assertEquals(cleanUpResponseEntity.getStatusCode(), HttpStatus.OK);
        assertNull(cleanUpResponseEntity.getBody());

        // test no data left
        getResponseEntity =
                restTemplate.exchange(getUri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {});

        assertEquals(getResponseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(getResponseEntity.getBody());
        assertEquals(0, getResponseEntity.getBody().size());
    }

}

package com.echipadesoc.easyorder.integration;

import com.echipadesoc.easyorder.domain.model.dtos.MenuItemDTO;
import com.echipadesoc.easyorder.domain.model.dtos.MenuSectionDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MenuControllerIT {

    @Value("${spring.rest.base-url}")
    private String baseUrl;

    @Value("${spring.rest.menu-path}")
    private String menuPath;

    @Value("${spring.rest.items-path}")
    private String itemsPath;

    @Value("${spring.rest.login-path}")
    private String loginPath;

    private RestTemplate restTemplate;

    private TestUtils testUtils;

    @Before
    public void init() {
        this.restTemplate = new RestTemplate();
        this.testUtils = new TestUtils(baseUrl, loginPath);
    }

    @Test
    public void getMenu_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .build()
                .toUri();
        ResponseEntity<List<MenuSectionDTO>> responseEntity = restTemplate
                .exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {});

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<MenuSectionDTO> response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void updateMenuItem_Ok() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-1")
                .build()
                .toUri();
        MenuItemDTO menuItemDTO = new MenuItemDTO(-1, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestUpdate = new HttpEntity<>(menuItemDTO, testUtils.getAccessHeadersManager());

        ResponseEntity<MenuItemDTO> responseEntity =
                restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(menuItemDTO.getName(), responseEntity.getBody().getName());
    }

    @Test
    public void updateMenuItem_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-1")
                .build()
                .toUri();
        MenuItemDTO menuItemDTO = new MenuItemDTO(-1, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestUpdate = new HttpEntity<>(menuItemDTO);

        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {
                    });

            assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void updateMenuItem_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-100")
                .build()
                .toUri();
        MenuItemDTO menuItemDTO = new MenuItemDTO(-1, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestUpdate = new HttpEntity<>(menuItemDTO, testUtils.getAccessHeadersManager());

        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, new ParameterizedTypeReference<>() {
                    });

            assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void addMenuItem_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        MenuItemDTO menuItemDTO = new MenuItemDTO(-1, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestAdd = new HttpEntity<>(menuItemDTO);
        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.POST, requestAdd, new ParameterizedTypeReference<>() {
                    });
            assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void getMenuItems_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();

        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());

        ResponseEntity<List<MenuItemDTO>> responseEntity =
                restTemplate.exchange(uri, HttpMethod.GET, requestGetMenuItems, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
    }

    @Test
    public void getMenuItems_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();

        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null);

        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, requestGetMenuItems, new ParameterizedTypeReference<>() {
                    });

            assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void getMenuItems_NoQueryParams() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString());
        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        ResponseEntity<List<MenuItemDTO>> responseEntity =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestGetMenuItems,
                                      new ParameterizedTypeReference<>() {
                                      });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<MenuItemDTO> response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void getMenuItems_PageAndSizeParams() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString())
                .queryParam("page", 1)
                .queryParam("size", 3);
        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        ResponseEntity<List<MenuItemDTO>> responseEntity =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestGetMenuItems, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<MenuItemDTO> response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void getMenuItems_PageAndSizeParamsBadRequest() {//page null and Size not null
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString())
                .queryParam("size", 3);
        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        try {
            ResponseEntity<List<MenuItemDTO>> responseEntity =
                    restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestGetMenuItems, new ParameterizedTypeReference<>() {
                    });
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void getMenuItems_PageSizeAndSortByParams() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString())
                .queryParam("page", 1)
                .queryParam("size", 3)
                .queryParam("sortBy", "name.asc,price.desc");
        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        ResponseEntity<List<MenuItemDTO>> responseEntity =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestGetMenuItems, new ParameterizedTypeReference<>() {
                });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<MenuItemDTO> response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void getMenuItems_OnlySortByParam() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString())
                .queryParam("sortBy", "name.asc,price.desc");
        HttpEntity<String> requestGetMenuItems = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        ResponseEntity<List<MenuItemDTO>> responseEntity =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestGetMenuItems,
                                      new ParameterizedTypeReference<>() {
                                      });

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<MenuItemDTO> response = responseEntity.getBody();
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void getMenuItemById_OK() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-1")
                .build()
                .toUri();
        HttpEntity<MenuItemDTO> requestGetMenuItemById = new HttpEntity<>(null, testUtils.getAccessHeadersManager());

        ResponseEntity<MenuItemDTO> responseEntity =
                restTemplate.exchange(uri, HttpMethod.GET, requestGetMenuItemById, new ParameterizedTypeReference<>() {
                });
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
    }

    @Test
    public void getMenuItemById_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-1")
                .build()
                .toUri();
        MenuItemDTO menuItemDTO = new MenuItemDTO(-1, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestGetMenuItemById = new HttpEntity<>(menuItemDTO);

        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, requestGetMenuItemById, new ParameterizedTypeReference<>() {
                    });

            assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void getMenuItemById_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-100")
                .build()
                .toUri();
        HttpEntity<MenuItemDTO> requestGetMenuItemById = new HttpEntity<>(null, testUtils.getAccessHeadersManager());

        try {
            ResponseEntity<MenuItemDTO> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, requestGetMenuItemById, new ParameterizedTypeReference<>() {
                    });

            assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void addMenuItem_OK_deleteMenuItem_OK() {
        //addMenuItem_OK
        URI addUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .build()
                .toUri();

        MenuItemDTO menuItemDTO = new MenuItemDTO(0L, "name", "description", 3F, "photoPath", null, true, "salad");
        HttpEntity<MenuItemDTO> requestAddMenuItem = new HttpEntity<>(menuItemDTO, testUtils.getAccessHeadersManager());
        ResponseEntity<MenuItemDTO> addResponseEntity = restTemplate.exchange(addUri, HttpMethod.POST, requestAddMenuItem,
                                                                              new ParameterizedTypeReference<>() {});
        assertEquals(addResponseEntity.getStatusCode(), HttpStatus.OK);

        //deleteMenuItem_OK
        URI deleteUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment(String.valueOf(Objects.requireNonNull(addResponseEntity.getBody()).getId()))
                .build()
                .toUri();

        HttpEntity<Void> requestDeleteMenuItem = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        ResponseEntity<Void> deleteResponseEntity = restTemplate.exchange(deleteUri, HttpMethod.DELETE, requestDeleteMenuItem,
                                                                          new ParameterizedTypeReference<>() {});
        assertEquals(deleteResponseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void deleteMenuItem_Forbidden() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-3")
                .build()
                .toUri();

        try {
            ResponseEntity<Void> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.DELETE, null, new ParameterizedTypeReference<>() {});
            assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        } catch (HttpClientErrorException e) {
            assertEquals(403, e.getRawStatusCode());
        }
    }

    @Test
    public void deleteMenuItem_NotFound() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-100")
                .build()
                .toUri();

        HttpEntity<Void> requestDeleteMenuItem = new HttpEntity<>(null, testUtils.getAccessHeadersManager());
        try {
            ResponseEntity<Void> deleteResponseEntity =
                    restTemplate.exchange(uri, HttpMethod.DELETE, requestDeleteMenuItem, new ParameterizedTypeReference<>() {});
            assertEquals(deleteResponseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
        } catch (HttpClientErrorException e) {
            assertEquals(404, e.getRawStatusCode());
        }
    }

    @Test
    public void deleteMenuItem_UsedByOrder() {
        URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .pathSegment(menuPath)
                .pathSegment(itemsPath)
                .pathSegment("-1")
                .build()
                .toUri();
        HttpEntity<Void> requestDeleteMenuItem = new HttpEntity<>(null, testUtils.getAccessHeadersManager());

        try {
            ResponseEntity<Void> responseEntity =
                    restTemplate.exchange(uri, HttpMethod.DELETE, requestDeleteMenuItem, new ParameterizedTypeReference<>() {});
            assertEquals(responseEntity.getStatusCode(), HttpStatus.CONFLICT);
        } catch (HttpClientErrorException e) {
            assertEquals(409, e.getRawStatusCode());
        }
    }
}
